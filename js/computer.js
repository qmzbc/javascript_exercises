// 获取html元素
// 输入框1
var num1 = document.getElementById('number1')
// 输入框2
var num2 = document.getElementById('number2')
// 选择
var select = document.getElementById('fuhao')
// 计算
var obtn = document.getElementById('btn')
// 结果
var result = document.getElementById('result-box')

// 绑定事件，点击计算时执行函数
obtn.onclick = function () {
  if (select.value === "add") {   //选择+号的情况下
    var onum1 = num1.value
    var onum2 = num2.value
    var one = parseFloat(onum1)   //转化为数字类型
    var two = parseFloat(onum2) 
    var oadd = one + two
    result.innerHTML = oadd   //打印在result盒子中
  } else if (select.value === "from") {   //选择-号的情况下
    var ofrom = num1.value - num2.value
    result.innerHTML = ofrom    //打印在result盒子中
  } else if (select.value === "times") {    //选择*号的情况下
    var otimes = num1.value * num2.value
    result.innerHTML = otimes   //打印在result盒子中
  } else if (select.value === "into") {   //选择/号的情况下
    var ointo = num1.value / num2.value
    result.innerHTML = ointo    //打印在result盒子中
  }
}

